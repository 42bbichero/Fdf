/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/22 15:34:14 by bbichero          #+#    #+#             */
/*   Updated: 2015/04/22 15:34:15 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_env			*ft_error_gnl(t_env *e, int err, char *file)
{
	struct stat		sta;

	stat(file, &sta);
	e->error = 0;
	if (S_ISDIR(sta.st_mode) == 1)
	{
		e->error = 1;
		ft_putendl("Please give me a map not a directory");
		return (e);
	}
	if (err < 1)
	{
		e->error = 1;
		ft_putendl("Please give me some valid arguments");
		return (e);
	}
	return (e);
}
